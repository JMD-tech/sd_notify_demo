
// First before compile: apt install libsystemd-dev

#include <iostream>
#include <unistd.h>
#include <systemd/sd-daemon.h>

using std::cout; using std::endl;

int main(int argc, char** argv)
{
	cout << "sd_notify test started" << endl;
	
	if (argc > 1)
	{
		int pause = atoi(argv[1]);
		cout << "Simulating initialization time of " << pause << " seconds." << endl;
		sd_notify(0,"STATUS=Initializing...");	// Optional, this gets displayed in "Status" line of systemctl status
		sleep(pause);
	}
	
	cout << "initialization done, entering main loop" << endl;
	sd_notify(0,"READY=1\nSTATUS=We're ready to rumble!");
	// Now systemd can start other services marked After= us
	
	while(1)
		sleep(1);
	
	cout << "exiting" << endl;
	
	return 0;
}
