
CXXFLAGS=$(CXXOPTFLAGS) -Wall -std=c++14

LDFLAGS+= -lsystemd

PROGNAME=sdnotest

OBJS=$(PROGNAME).o

default: all

all: $(PROGNAME)

clean:
	rm -f *.o *.exe $(PROGNAME)

$(PROGNAME): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(PROGNAME) $(OBJS) $(LDFLAGS)

install: $(PROGNAME)
	install -d $(DESTDIR)/opt/sd_notify
	install -m 755 sdnotest $(DESTDIR)/opt/sd_notify/

install_service: install
	install -m 644 sdnotest.service $(DESTDIR)/etc/systemd/system/
	install -m 644 sdnotest-dependant.service $(DESTDIR)/etc/systemd/system/
	install -m 644 another.service $(DESTDIR)/etc/systemd/system/
	install -m 644 another-dependant.service $(DESTDIR)/etc/systemd/system/
	systemctl daemon-reload
	systemctl enable sdnotest
	systemctl enable sdnotest-dependant
	systemctl enable another
	systemctl enable another-dependant

